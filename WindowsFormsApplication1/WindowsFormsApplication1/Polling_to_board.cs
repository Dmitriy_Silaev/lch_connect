﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.IO;
using System.Xml.Serialization;
using System.Net;
using System.Net.Sockets;
using System.Diagnostics;
namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {

       
        Stopwatch OSW = new Stopwatch();
        Stopwatch MOSW = new Stopwatch();
        private void Polling1()
        {
            polling_fn(mtxcom1, serialPort1, groupBox5,1);
        }
        private void Polling2()
        {
            polling_fn(mtxcom2, serialPort2, groupBox5,2);
        }

        private void Polling3()
        {
            polling_fn(mtxcom3, serialPort3, groupBox5,3);
        }

        private void Polling4()
        {
            polling_fn(mtxcom4, serialPort4, groupBox5,4);
        }

       


        private void polling_fn( Mutex mtxcom, System.IO.Ports.SerialPort serialPort, GroupBox groupBox, byte address)
        {
            byte[] InBuffer = new byte[1024];
            byte[] MidBuffer = new byte[1024];
            int Count = 0;
            
            int u = 0;
           
           

            while (true)
            {

                Stopwatch SW = new Stopwatch();
                Count = 0;
                if (serialPort.IsOpen)
                {

                    try
                    {
                        serialPort.ReadTimeout = -1;
                        InBuffer[0] = (byte)serialPort.ReadByte();
                    }
                    catch (Exception ex)
                    {
                        serialPort.ReadTimeout = -1;
                        continue;
                    }
                    Count++;
                    mtxcom.WaitOne();
                    try
                    {
                        //serialPort.ReadTimeout = 50;
                        while (serialPort.BytesToRead != 0)
                        {

                            byte[] data = new byte[serialPort.BytesToRead];
                            serialPort.Read(data, 0, data.Length);
                            Array.Copy(data, 0, InBuffer, Count, data.Length);
                            Count += data.Length;
                            Thread.Sleep(10);
                        }
                    }
                    //try
                    //{
                    //    serialPort.ReadTimeout = 10;
                    //    byte[] data = new byte[1024];
                    //    for (int i = 1; i < 1024; i++)
                    //    {
                    //        InBuffer[i] = (byte)serialPort.ReadByte();
                    //        Count++;
                    //    }
                    //}
                    catch (Exception ex)
                    {
                        serialPort.ReadTimeout = -1;
                        mtxcom.ReleaseMutex();
                        continue;
                    }
                    mtxcom.ReleaseMutex();


                    //Count = 0;
                    //try
                    //{
                    //    serialPort.ReadTimeout = 5000;
                    //    InBuffer[0] = (byte)serialPort.ReadByte();
                    //    Thread.Sleep(1);
                    //}
                    //catch (Exception ex)
                    //{
                    //    serialPort.ReadTimeout = 5000;
                    //    Thread.Sleep(500);
                    //    continue;
                    //}
                    //Count++;



                    //mtxcom.WaitOne();                 
                    //try
                    //{
                    //    serialPort.ReadTimeout = 30;
                    //    byte[] data = new byte[1024];

                    //    for (int i = 1; i < 1024; i++)
                    //    {                
                    //        InBuffer[i] = (byte)serialPort.ReadByte();
                    //        Count++;

                    //    }
                    //}
                    //catch (Exception ex)
                    //{
                    //    serialPort.ReadTimeout = 30;
                    //}
                    //mtxcom.ReleaseMutex();





                    int port = 0;
                    try
                    {
                        IPEndPoint client = (IPEndPoint)socksender.RemoteEndPoint;
                        port = client.Port;
                        //Console.WriteLine("Connected with {0} at port {1}", newclient.Address, newclient.Port);
                    }
                    catch
                    {
                        continue;
                    }
                    if ((port == 6000))
                    {
                        if (serialPort == serialPort4)
                        {
                            try
                            {
                                socksender.Send(InBuffer, Count, SocketFlags.None);
                            }
                            catch
                            {
                                groupBox.Invoke(new Action<System.Drawing.Color>(t => groupBox5.BackColor = t), Color.LightPink);
                                Count = 0;
                            }
                        }
                    }
                    else
                    {
                        try
                        {
                            if (Count > 0)
                            {


                                ////////////////////формируем пакет////////////////////////////
                                MidBuffer[0] = 0x80;
                                MidBuffer[1] = 0x80;
                                MidBuffer[2] = 0x80;
                                MidBuffer[3] = 0x01;
                                MidBuffer[4] = 0x00;//длинна пакета
                                MidBuffer[5] = 0x00;//длинна пакета
                                MidBuffer[6] = GlobalHostNum;//адрес отправителя
                                MidBuffer[7] = address;//адрес получателя
                                MidBuffer[8] = 0x00;//номер 
                                MidBuffer[9] = 0x00;//пакета
                                MidBuffer[10] = 0x03;//ф-я запись
                                MidBuffer[11] = 0x01;//кол-во индекс команд
                                MidBuffer[12] = 0x20;//индексированная
                                MidBuffer[13] = 0x03;//команда
                                MidBuffer[14] = 0x01;//номер
                                MidBuffer[15] = 0x00;//структуры
                                MidBuffer[16] = (byte)(Count);//длинна
                                MidBuffer[17] = (byte)(Count >> 8);   //данных
                                for (u = 0; u < Count; u++)
                                {
                                    MidBuffer[18 + u] = InBuffer[u];
                                }

                                MidBuffer[4] = (byte)((18 + u + 1 + 1));//длинна пакета
                                MidBuffer[5] = (byte)((18 + u + 1 + 1) >> 8);//длинна пакета

                                int my_crc16 = CRC_16(MidBuffer, (18 + u), 0);
                                MidBuffer[18 + u] = (byte)my_crc16;
                                MidBuffer[18 + u + 1] = (byte)(my_crc16 >> 8);


                                lock (obj)
                                {
                                    if (socksender != null)
                                    {
                                        if (socksender.Connected != false)
                                        {

                                            OSW.Reset();
                                            OSW.Start();
                                            socksender.Send(MidBuffer, (18 + u + 1 + 1), SocketFlags.None);
                                            /*
                                            char[] msg = {'+','V','E','R','_','I','N','F','O','=','?','\r','\n'};
                                            byte[] sendmsg = new byte[13];
                                            for(int i = 0; i<13; i++)
                                            {
                                                sendmsg[i] = (byte)msg[i];
                                            }
                                            socksender.Send(sendmsg, 13, SocketFlags.None);

    */
                                            /*                       
                                                                                        MOSW.Stop(); //Останавливаем
                                                                                        label9.Invoke(new Action<String>(t => label9.Text = t), Convert.ToString(MOSW.ElapsedMilliseconds));
                                                                                        long a = MOSW.ElapsedMilliseconds;
                                                                                        if (a > bb)
                                                                                        {
                                                                                            bb = a;
                                                                                            label10.Invoke(new Action<String>(t => label10.Text = t), Convert.ToString(bb));
                                                                                        }

                                            */
                                        }
                                    }
                                }
                                Count = 0;
                            }
                        }
                        catch (SocketException e)
                        {
                            groupBox.Invoke(new Action<System.Drawing.Color>(t => groupBox5.BackColor = t), Color.LightPink);
                            Count = 0;
                        }
                    }
                }
                else
                {
                    Thread.Sleep(50);
                }

            }
        }

 
    }
}