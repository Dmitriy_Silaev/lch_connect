﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.IO;
using System.Xml.Serialization;
using System.Net;
using System.Net.Sockets;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        public uint Com1PortExist = 0;
        public uint Com2PortExist = 0;
        public uint Com3PortExist = 0;
        public uint Com4PortExist = 0;

        public uint SocketConnected = 0;
        //public uint SocketOnStartValid = 0;
        public byte GlobalHostNum = 0;

        public static Mutex mtxcom1 = new Mutex();
        public static Mutex mtxcom2 = new Mutex();
        public static Mutex mtxcom3 = new Mutex();
        public static Mutex mtxcom4 = new Mutex();
        public static Mutex mtxlan = new Mutex();
        object obj = new object();
        Queue<byte> qe = new Queue<byte>();

        public Form1()
        {
            InitializeComponent();
        }

       
        IPHostEntry ipHost; //= Dns.GetHostEntry("127.0.0.1");//"localhost"
        IPAddress ipAddr;// = IPAddress.Parse(ipfromtextbox);//ipHost.AddressList[0];"10.185.2.152""192.168.0.120"
        IPEndPoint ipEndPoint;// = new IPEndPoint(ipAddr, portfromtextbox);

        Socket socksender;// = new Socket(ipAddr.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
        //var pollingThread5;
        //var pollingThread = new Thread(Polling6);
        Thread GetFromSocketThread;

        Thread ToTop1;
        Thread ToTop2;
        Thread ToTop3;
        Thread ToTop4;

        string ipfromtextbox;
        int portfromtextbox;
        private void Form1_Load(object sender, EventArgs e)
        {

            label9.Visible = false;
            label10.Visible = false;
            label11.Visible = false;
            label12.Visible = false;

            // передаем в конструктор тип класса
            XmlSerializer formatter = new XmlSerializer(typeof(Com));
      

            //System.IO.Ports.SerialPort Port;
            //foreach (string str in System.IO.Ports.SerialPort.GetPortNames())
            //{
            //    try
            //    {
            //        Port = new System.IO.Ports.SerialPort(str);
            //        //Открываем новое соединение последовательного порта.
            //        Port.Open();

            //        //Выполняем проверку полученного порта
            //        //true, если последовательный порт открыт, в противном случае — false.
            //        //Значение по умолчанию — false.
            //        if (Port.IsOpen)
            //        {
            //            //Если порт открыт то добавляем его в listBox
            //            listBox1.Items.Add(str);
            //            //listBox1.SetSelected(0, true);
            //            //listBox1.SelectedItem = serialPort1.PortName;

            //            listBox2.Items.Add(str);
            //            //listBox2.SetSelected(0, true);
            //            //listBox2.SelectedItem = serialPort2.PortName;

            //            listBox3.Items.Add(str);
            //            //listBox3.SetSelected(0, true);
            //            //listBox3.SelectedItem = serialPort3.PortName;

            //            listBox4.Items.Add(str);
            //            //listBox4.SetSelected(0, true);
            //            //listBox4.SelectedItem = serialPort4.PortName;
            //            //Уничтожаем внутренний объект System.IO.Stream.
            //            Port.Close();

            //            //возвращаем состояние получения портов
            //            //available = true;
            //        }
            //    }
            //    //Ловим все ошибки и отображаем, что открытых портов не найдено               
            //    catch (Exception ex)
            //    {
            //        MessageBox.Show(ex.ToString(), "Ошибка при сканировании!",
            //               MessageBoxButtons.OK, MessageBoxIcon.Error);
            //    }
            //}



            Com newPerson;

            string port1 = "";
            int baud1 = 0;
            string port2 = "";
            int baud2 = 0;
            string port3 = "";
            int baud3 = 0;
            string port4 = "";
            int baud4 = 0;
            string ipaddr = "";
            int port = 0;
            int host = 0;
            // десериализация
            using (FileStream fs = new FileStream("config.xml", FileMode.OpenOrCreate))
            {
             
                try
                {
                    newPerson = (Com)formatter.Deserialize(fs);
                    port1 = newPerson.Name1;
                    baud1 = newPerson.Baudrate1;
                    port2 = newPerson.Name2;
                    baud2 = newPerson.Baudrate2;
                    port3 = newPerson.Name3;
                    baud3 = newPerson.Baudrate3;
                    port4 = newPerson.Name4;
                    baud4 = newPerson.Baudrate4;
                    ipaddr = newPerson.IpNum;
                    port = newPerson.Port;
                    host = newPerson.HostNum;
                }
                catch
                {
                    groupBox1.BackColor = Color.LightCoral;
                    groupBox2.BackColor = Color.LightCoral;
                    groupBox3.BackColor = Color.LightCoral;
                    groupBox4.BackColor = Color.LightCoral;
                }

                try
                {

                    if (!serialPort1.IsOpen)
                    {
                        serialPort1.PortName = port1;
                        serialPort1.BaudRate = baud1;//comboBox1.SelectedItem.ToString();
                        serialPort1.Open();
                        Com1PortExist = 1;
                        label1.Text = serialPort1.PortName;
                        label2.Text = serialPort1.BaudRate.ToString();
                        comboBox1.Text = serialPort1.BaudRate.ToString();
                    }
                }
                catch
                {
                    groupBox1.BackColor = Color.LightPink;
                }

                try
                {
                    if (!serialPort2.IsOpen)
                    {
                        serialPort2.PortName = port2;
                        serialPort2.BaudRate = baud2;//comboBox1.SelectedItem.ToString();
                        serialPort2.Open();
                        Com2PortExist = 1;
                        label3.Text = serialPort2.PortName;
                        label4.Text = serialPort2.BaudRate.ToString();
                        comboBox2.Text = serialPort2.BaudRate.ToString();
                    }
                }
                catch
                {
                    groupBox2.BackColor = Color.LightPink;
                }

                try
                {
                    if (!serialPort3.IsOpen)
                    {
                        serialPort3.PortName = port3;
                        serialPort3.BaudRate = baud3;//comboBox1.SelectedItem.ToString();
                        serialPort3.Open();
                        Com3PortExist = 1;
                        label5.Text = serialPort3.PortName;
                        label6.Text = serialPort3.BaudRate.ToString();
                        comboBox3.Text = serialPort3.BaudRate.ToString();
                    }
                }
                catch
                {
                    groupBox3.BackColor = Color.LightPink;
                }

                try
                {
                    if (!serialPort4.IsOpen)
                    {
                        serialPort4.PortName = port4;
                        serialPort4.BaudRate = baud4;//comboBox1.SelectedItem.ToString();
                        serialPort4.Open();
                        Com4PortExist = 1;
                        label7.Text = serialPort4.PortName;
                        label8.Text = serialPort4.BaudRate.ToString();
                        comboBox4.Text = serialPort4.BaudRate.ToString();
                    }
                }
                catch
                {
                    groupBox4.BackColor = Color.LightPink;
                }
                try
                {
                    textBox1.Text = ipaddr;
                    textBox2.Text = port.ToString();//comboBox1.SelectedItem.ToString();
                    comboBox5.Text = host.ToString();
                    GlobalHostNum = (byte)host;

                }
                  catch 
                {
                    //MessageBox.Show("Настройте конфигурацию");
                    //Close();
                    groupBox1.BackColor = Color.LightCoral;
                    groupBox2.BackColor = Color.LightCoral;
                    groupBox3.BackColor = Color.LightCoral;
                    groupBox4.BackColor = Color.LightCoral;
                }
           
          

            }


            //----------------------------------------родной код-------------------------------------------------          
            try
            {
                string[] str = System.IO.Ports.SerialPort.GetPortNames();

                listBox1.Items.AddRange(str);
                listBox1.SetSelected(0, true);
                listBox1.SelectedItem = serialPort1.PortName;



                listBox2.Items.AddRange(str);
                listBox2.SetSelected(0, true);
                listBox2.SelectedItem = serialPort2.PortName;



                listBox3.Items.AddRange(str);
                listBox3.SetSelected(0, true);
                listBox3.SelectedItem = serialPort3.PortName;



                listBox4.Items.AddRange(str);
                listBox4.SetSelected(0, true);
                listBox4.SelectedItem = serialPort4.PortName;
                //Представляет ресурс последовательного порта.



            }
            catch (ArgumentOutOfRangeException)
            {
                MessageBox.Show("Подключите устройство к порту (нет ком порта)");
                Close();
            }
            //-------------------------------------------------------------------------------------------------------------------
            //--------------------------------------не родной код, отобразает только не занятые порты----------------------------------------------------
            //System.IO.Ports.SerialPort Port;
            //foreach (string str in System.IO.Ports.SerialPort.GetPortNames())
            //{
            //    try
            //    {
            //        Port = new System.IO.Ports.SerialPort(str);
            //        //Открываем новое соединение последовательного порта.
            //        Port.Open();

            //        //Выполняем проверку полученного порта
            //        //true, если последовательный порт открыт, в противном случае — false.
            //        //Значение по умолчанию — false.
            //        if (Port.IsOpen)
            //        {
            //            //Если порт открыт то добавляем его в listBox
            //            listBox1.Items.Add(str);
            //            listBox1.SetSelected(0, true);
            //            listBox1.SelectedItem = serialPort1.PortName;

            //            listBox2.Items.Add(str);
            //            listBox2.SetSelected(0, true);
            //            listBox2.SelectedItem = serialPort2.PortName;

            //            listBox3.Items.Add(str);
            //            listBox3.SetSelected(0, true);
            //            listBox3.SelectedItem = serialPort3.PortName;

            //            listBox4.Items.Add(str);
            //            listBox4.SetSelected(0, true);
            //            listBox4.SelectedItem = serialPort4.PortName;
            //            //Уничтожаем внутренний объект System.IO.Stream.
            //            Port.Close();

            //            //возвращаем состояние получения портов
            //            //available = true;
            //        }
            //    }
            //    //Ловим все ошибки и отображаем, что открытых портов не найдено               
            //    catch (Exception ex)
            //    {
            //        //MessageBox.Show(ex.ToString(), "Ошибка при сканировании!",
            //        //       MessageBoxButtons.OK, MessageBoxIcon.Error);
            //    }
            //}
            //-----------------------------------------------------------------------------------------------------
            if (serialPort1.IsOpen)
            {
                var pollingThread = new Thread(Polling1);
                pollingThread.IsBackground = true;
                pollingThread.Start();
            }

            if (serialPort2.IsOpen)
            {
                var pollingThread = new Thread(Polling2);
                pollingThread.IsBackground = true;
                pollingThread.Start();
            }

            if (serialPort3.IsOpen)
            {
                var pollingThread = new Thread(Polling3);
                pollingThread.IsBackground = true;
                pollingThread.Start();
            }

            if (serialPort4.IsOpen)
            {
                var pollingThread = new Thread(Polling4);
                pollingThread.IsBackground = true;
                pollingThread.Start();
            }
//////////////////////////////////////////////////////////////////////////
          
            GetFromSocketThread = new Thread(this.Polling6);
            GetFromSocketThread.IsBackground = true;
            GetFromSocketThread.Start();

            ToTop1 = new Thread(this.PollingTop1);
            ToTop1.IsBackground = true;
            ToTop1.Start();

            ToTop2 = new Thread(this.PollingTop2);
            ToTop2.IsBackground = true;
            ToTop2.Start();

            ToTop3 = new Thread(this.PollingTop3);
            ToTop3.IsBackground = true;
            ToTop3.Start();

           ToTop4 = new Thread(this.PollingTop4);
           ToTop4.IsBackground = true;
           ToTop4.Start();
          

            //using (FileStream fs5 = new FileStream("ipport.xml", FileMode.OpenOrCreate))
            //{
            //    try
            //    {
            //        Ip ipPerson = (Ip)formatter5.Deserialize(fs5);

            //            textBox1.Text = ipPerson.IpNum;
            //            textBox2.Text = ipPerson.Port.ToString();//comboBox1.SelectedItem.ToString();
            //            comboBox5.Text = ipPerson.HostNum.ToString();
            //            GlobalHostNum = (byte)ipPerson.HostNum;
            //            //GlobalHostNum = 0xFF;//test
 
            //            //int hostnum = int.Parse(comboBox5.SelectedItem.ToString());
            //            //serialPort4.Open();
            //            //Com4PortExist = 1;
            //            //label7.Text = serialPort4.PortName;
            //            //label8.Text = serialPort4.BaudRate.ToString();
            //            //comboBox4.Text = serialPort4.BaudRate.ToString();
                    
            //    }
            //    catch (ArgumentNullException)
            //    {
            //        groupBox5.BackColor = Color.LightPink;
            //    }
            //    catch (InvalidOperationException)
            //    {
            //        //MessageBox.Show("Настройте конфигурацию");
            //        //Close();
            //        groupBox5.BackColor = Color.LightPink;
            //    }
            //    catch (ArgumentOutOfRangeException)
            //    {
            //        // MessageBox.Show("Подключите устройство к порту (нет ком порта)");
            //        // Close();
            //        groupBox5.BackColor = Color.LightPink;
            //    }
            //    catch (UnauthorizedAccessException)
            //    {
            //        //MessageBox.Show("Отказ в доступе к порту load");
            //        groupBox5.BackColor = Color.LightPink;
            //    }
            //    catch (IOException)
            //    {
            //        //MessageBox.Show("Отсутствует ком порт");
            //        //Close();
            //        groupBox5.BackColor = Color.LightPink;
            //    }

            //}

            lock (obj)
            {
                try
                {
                    //mtxlan.WaitOne();

                    //запускаем поток контролиррующий сокет
                    var pollingThread5 = new Thread(Polling5);
                    pollingThread5.IsBackground = true;
                    pollingThread5.Start();

                    ipfromtextbox = textBox1.Text;
                    portfromtextbox = int.Parse(textBox2.Text);

                    // Устанавливаем удаленную точку для сокета
                    ipHost = Dns.GetHostEntry("127.0.0.1");//"localhost"
                    ipAddr = IPAddress.Parse(ipfromtextbox);//ipHost.AddressList[0];"10.185.2.152""192.168.0.120"
                    ipEndPoint = new IPEndPoint(ipAddr, portfromtextbox);

                    socksender = new Socket(ipAddr.AddressFamily, SocketType.Stream, ProtocolType.Tcp);

                    // Соединяем сокет с удаленной точкой
                    socksender.Connect(ipEndPoint);
                    //SocketConnected = 1;
                    //GetFromSocketThread.Resume();
                    groupBox5.BackColor = System.Drawing.SystemColors.Control;


                    //GetFromSocketThread.Resume();


                    // Освобождаем сокет
                    //socksender.Shutdown(SocketShutdown.Both);
                    //socksender.Close();

                }
                catch (Exception ex)
                {
                    //Console.WriteLine(ex.ToString());
                    groupBox5.BackColor = Color.LightPink;
                    //SocketConnected = 0;
                    //GetFromSocketThread.Suspend();
                }
                finally
                {
                    //Console.ReadLine();

                }
                //mtxlan.ReleaseMutex();
            }
        }

     


        private void button1_Click(object sender, EventArgs e)//открыть порт
        {

             try
            {
                //mtxcom2.WaitOne();
                //if (serialPort2.IsOpen) serialPort2.Close();
                //mtxcom2.ReleaseMutex();
                //mtxcom3.WaitOne();
                //if (serialPort3.IsOpen) serialPort3.Close();
                //mtxcom3.ReleaseMutex();
                //mtxcom4.WaitOne();
                //if (serialPort4.IsOpen) serialPort4.Close();
                //mtxcom4.ReleaseMutex();
              
                    mtxcom1.WaitOne();
                if (serialPort1.IsOpen)
                {
                    serialPort1.Dispose();
                    serialPort1.Close();
                }
                    ///mtxcom1.ReleaseMutex();
                  
                    //mtxcom1.ReleaseMutex();
                    //mtxcom1.WaitOne();
                    serialPort1.PortName = listBox1.SelectedItem.ToString();
                    serialPort1.BaudRate = int.Parse(comboBox1.SelectedItem.ToString());//comboBox1.SelectedItem.ToString();
                    mtxcom1.ReleaseMutex();
                    mtxcom1.WaitOne();

                    serialPort1.Open();
                    label1.Text = serialPort1.PortName;
                    label2.Text = serialPort1.BaudRate.ToString();

                    groupBox1.BackColor = System.Drawing.SystemColors.Control;








                    //// объект для сериализации
                    //Com com = new Com(serialPort1.PortName, serialPort1.BaudRate);
                    ////Console.WriteLine("Объект создан");

                    //// передаем в конструктор тип класса
                    //XmlSerializer formatter = new XmlSerializer(typeof(Com));

                    //// получаем поток, куда будем записывать сериализованный объект
                    //using (FileStream fs = new FileStream("comone.xml", FileMode.Truncate))
                    //{
                    //    formatter.Serialize(fs, com);
                    //}

                    mtxcom1.ReleaseMutex();
               

            }
             catch (Exception ex)//
             {               
                 groupBox1.BackColor = Color.LightPink;
                 mtxcom1.ReleaseMutex();             
             }
            


             try
             {

                 mtxcom2.WaitOne();
                
                if (serialPort2.IsOpen)
                {
                    serialPort2.Dispose();
                    serialPort2.Close();
                }
                //mtxcom2.ReleaseMutex();


                serialPort2.PortName = listBox2.SelectedItem.ToString();
                     serialPort2.BaudRate = int.Parse(comboBox2.SelectedItem.ToString());//comboBox1.SelectedItem.ToString();

                     //mtxcom2.WaitOne();

                     serialPort2.Open();
                     label3.Text = serialPort2.PortName;
                     label4.Text = serialPort2.BaudRate.ToString();
                     groupBox2.BackColor = System.Drawing.SystemColors.Control;
                     //// объект для сериализации
                     //Com com = new Com(serialPort2.PortName, serialPort2.BaudRate);
                     ////Console.WriteLine("Объект создан");

                     //// передаем в конструктор тип класса
                     //XmlSerializer formatter = new XmlSerializer(typeof(Com));

                     //// получаем поток, куда будем записывать сериализованный объект
                     //using (FileStream fs = new FileStream("comtwo.xml", FileMode.Truncate))
                     //{
                     //    formatter.Serialize(fs, com);
                     //}

                     mtxcom2.ReleaseMutex();
             }
             catch (Exception ex)
             {                 
                 groupBox2.BackColor = Color.LightPink;              
                 mtxcom2.ReleaseMutex();
             }
           
             try
             {

                 mtxcom3.WaitOne();
                if (serialPort3.IsOpen)
                {
                    serialPort3.Dispose();
                    serialPort3.Close();
                }
                //mtxcom3.ReleaseMutex();

                //mtxcom3.WaitOne();
                serialPort3.PortName = listBox3.SelectedItem.ToString();
                     serialPort3.BaudRate = int.Parse(comboBox3.SelectedItem.ToString());//comboBox1.SelectedItem.ToString();

                     serialPort3.Open();
                     label5.Text = serialPort3.PortName;
                     label6.Text = serialPort3.BaudRate.ToString();
                     groupBox3.BackColor = System.Drawing.SystemColors.Control;
                     //// объект для сериализации
                     //Com com = new Com(serialPort3.PortName, serialPort3.BaudRate);
                     ////Console.WriteLine("Объект создан");

                     //// передаем в конструктор тип класса
                     //XmlSerializer formatter = new XmlSerializer(typeof(Com));

                     //// получаем поток, куда будем записывать сериализованный объект
                     //using (FileStream fs = new FileStream("comthree.xml", FileMode.Truncate))
                     //{
                     //    formatter.Serialize(fs, com);
                     //}
                     mtxcom3.ReleaseMutex();

             }
             catch (Exception ex)
             {
                 groupBox3.BackColor = Color.LightPink;
                 mtxcom3.ReleaseMutex();
             }
        
             try
             {

                 mtxcom4.WaitOne();
                if (serialPort4.IsOpen)
                {
                    serialPort4.Dispose();
                    serialPort4.Close();
                }
                //mtxcom4.ReleaseMutex();

                //mtxcom4.WaitOne();
                serialPort4.PortName = listBox4.SelectedItem.ToString();
                     serialPort4.BaudRate = int.Parse(comboBox4.SelectedItem.ToString());//comboBox1.SelectedItem.ToString();

                     serialPort4.Open();
                     label7.Text = serialPort4.PortName;
                     label8.Text = serialPort4.BaudRate.ToString();
                     groupBox4.BackColor = System.Drawing.SystemColors.Control;
                     //// объект для сериализации
                     //Com com = new Com(serialPort4.PortName, serialPort4.BaudRate);
                     ////Console.WriteLine("Объект создан");

                     //// передаем в конструктор тип класса
                     //XmlSerializer formatter = new XmlSerializer(typeof(Com));

                     //// получаем поток, куда будем записывать сериализованный объект
                     //using (FileStream fs = new FileStream("comfour.xml", FileMode.Truncate))
                     //{
                     //    formatter.Serialize(fs, com);
                     //}
                     mtxcom4.ReleaseMutex();

             }
             catch (Exception ex)
             {
                 groupBox4.BackColor = Color.LightPink;
                 mtxcom4.ReleaseMutex();
             }

            int hostnum = 0;
                 try
                 {
                     //mtxlan.WaitOne();
                     lock (obj)
                     {
                         if (socksender != null)
                         {
                             if (socksender.Connected != false)
                             {
                                 // Освобождаем сокет

                                 socksender.Shutdown(SocketShutdown.Both);
                                 socksender.Close();

                             }
                         }
                     }
                     ipfromtextbox = textBox1.Text;
                     portfromtextbox = int.Parse(textBox2.Text);                  
                     hostnum = int.Parse(comboBox5.SelectedItem.ToString());
                  
                     //// объект для сериализации
                     //Ip ipport = new Ip(ipfromtextbox, portfromtextbox, hostnum);
                     ////Console.WriteLine("Объект создан");

                     //// передаем в конструктор тип класса
                     //XmlSerializer formatter = new XmlSerializer(typeof(Ip));

                     //// получаем поток, куда будем записывать сериализованный объект
                     //using (FileStream fs = new FileStream("ipport.xml", FileMode.Truncate))
                     //{
                     //    formatter.Serialize(fs, ipport);
                     //}
                     //mtxlan.ReleaseMutex();
                 }
                 catch (Exception ex)
                 {                   
                     groupBox5.BackColor = Color.LightPink;                 
                 }
            try
            {
                // объект для сериализации
                Com com = new Com(serialPort1.PortName, serialPort1.BaudRate, serialPort2.PortName, serialPort2.BaudRate,
                    serialPort3.PortName, serialPort3.BaudRate,
                    serialPort4.PortName, serialPort4.BaudRate,
                    ipfromtextbox, portfromtextbox, hostnum);
                //Console.WriteLine("Объект создан");

                // передаем в конструктор тип класса
                XmlSerializer formatter = new XmlSerializer(typeof(Com));

                // получаем поток, куда будем записывать сериализованный объект
                using (FileStream fs = new FileStream("config.xml", FileMode.Truncate))
                {
                    formatter.Serialize(fs, com);
                }

            }
            catch
            {

            }
        }


        static int CRC_16(byte[] bytes, int len, byte flag)//byte[] bytes//int CRC_16(byte* buffer, int len, byte flag)
        {
            int crc = 0xffff;
            int ind = 0;
            while (len > 0)
            {
                crc = crc16_update(crc, bytes[ind]);//crc=crc16_update(crc,*buffer)
                ind++;//buffer++
                len--;
            }
            if (flag > 0)
            {
                crc = crc & 0x0000;
                crc++;
            }
            return crc;
        }

        static int crc16_update(int crc, byte a)
        {
            int i;

            crc ^= a;
            for (i = 0; i < 8; ++i)
            {
                if ((crc & 1) != 0)
                    crc = (crc >> 1) ^ 0xA001;
                else
                    crc = (crc >> 1);
            }

            return crc;
        }
       [Serializable]

        public class Com
        {

            public string Name1 { get; set; }
            public int Baudrate1 { get; set; }
            public string Name2 { get; set; }
            public int Baudrate2 { get; set; }
            public string Name3 { get; set; }
            public int Baudrate3 { get; set; }
            public string Name4 { get; set; }
            public int Baudrate4 { get; set; }

            public string IpNum { get; set; }
            public int Port { get; set; }
            public int HostNum { get; set; }



            // стандартный конструктор без параметров
            public Com()
            { }

            public Com(string name1, int baudrate1,
                string name2, int baudrate2,
                string name3, int baudrate3,
                string name4, int baudrate4,
                string ipnum, int port, int hostnum)
            {
                Name1 = name1;
                Baudrate1 = baudrate1;

                Name2 = name2;
                Baudrate2 = baudrate2;

                Name3 = name3;
                Baudrate3 = baudrate3;

                Name4 = name4;
                Baudrate4 = baudrate4;

                IpNum = ipnum;
                Port = port;
                HostNum = hostnum;
            }
     
        }

       //public class Ip
       //{

       //    public string IpNum { get; set; }
       //    public int Port { get; set; }
       //    public int HostNum { get; set; }




       //    // стандартный конструктор без параметров
       //    public Ip()
       //    { }

       //    public Ip(string ipnum, int port, int hostnum)
       //    {

       //        IpNum = ipnum;
       //        Port = port;
       //        HostNum = hostnum;

       //    }
       //}
      
       private void comboBox3_SelectedIndexChanged(object sender, EventArgs e)
       {

       }


    }
}
