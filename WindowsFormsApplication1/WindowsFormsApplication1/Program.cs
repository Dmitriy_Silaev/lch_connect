﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace WindowsFormsApplication1
{
    //static class Program
    //{
    //    /// <summary>
    //    /// The main entry point for the application.
    //    /// </summary>
    //    /// 
    //    private static Mutex m_instance;
    //    private const string m_appName = "NameOfMyApp";

    //    [STAThread]
    //    static void Main()
    //    {
    //        //Application.EnableVisualStyles();
    //        //Application.SetCompatibleTextRenderingDefault(false);
    //        //Application.Run(new Form1());
    //        bool tryCreateNewApp;
    //        m_instance = new Mutex(true, m_appName,
    //                out tryCreateNewApp);
    //        if (tryCreateNewApp)
    //        {
    //            Application.EnableVisualStyles();
    //            Application.
    //              SetCompatibleTextRenderingDefault(false);
    //            Application.Run(new Form1());
    //            return;
    //        }
    //    }
    //}

    static class Program
    {
        [STAThread]

        [System.Runtime.InteropServices.DllImport("user32.dll")]
        [return: System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.Bool)]
        static extern bool SetForegroundWindow(IntPtr hWnd);

        [System.Runtime.InteropServices.DllImport("user32.dll")]
        [return: System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.Bool)]
        static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

        static void Main()
        {
            bool isNotRunning = true;
            using (var mutex = new System.Threading.Mutex(true, "МояПрограмма", out isNotRunning))
            {
                if (isNotRunning)
                {
                    Application.EnableVisualStyles();
                    Application.SetCompatibleTextRenderingDefault(false);
                    Application.Run(new Form1());
                }
                else
                {
                    var curr = System.Diagnostics.Process.GetCurrentProcess();
                    foreach (var proc in System.Diagnostics.Process.GetProcessesByName(curr.ProcessName))
                    {
                        if (proc.Id != curr.Id)
                        {
                            ShowWindow(proc.MainWindowHandle, 9);
                            ShowWindow(proc.MainWindowHandle, 5);
                            SetForegroundWindow(proc.MainWindowHandle);
                        }
                    }
                }
            }
        }
    }
}


